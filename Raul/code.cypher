// In order to start from scratch the following lines need to be run to DELETE
// all nodes from the Network:
//
// MATCH (n)
// DETACH DELETE n
//
// To run everything, Open Browser Settings and click on Enable multi statement
// query editor. Then copy and paste all this code.
// Constraint video IDs to be unique (if it doesn't exist already)
CREATE CONSTRAINT ON (v:Video) assert v.id is unique;
// Index is extremely important to make the loading faster
CREATE INDEX ON :Uploader(name);


/////////////////// LOADING VIDEOS ID /////////////////////////////////////////
// Loading video ID's and their properties (Category as a property)
// For small_tube: the code for the rest of the layers is the same
:auto USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///small_tube.csv" AS small
WITH small WHERE small.length IS NOT NULL
MERGE (v:Video {id: small.video_ID,
    length: toInteger(small.length),
    views: toInteger(small.views),
    rate: toFloat(small.rate),
    rating: toInteger(small.ratings),
    comments: toInteger(small.comments),
    age: toInteger(small.age),
    category: small.category})
MERGE (u:Uploader {name: small.uploader})
MERGE  (v)-[:UPLOADED_BY]->(u);

////////////////////////LOADING RELATED_TO USING STRENGHT AND AGE///////////////
// // Run the following line to delet all relationships
// MATCH ()-[r:RELATED_TO]->()
// DELETE r
// // create relation between video1 and video2
// // create relation between video1 and video2

// For small tube, the code for the rest of the layrs is the same
:auto USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///small_tube.csv" AS small
WITH small, small.video_ID AS videoID, small.related_1 AS rel_1,
small.related_2 AS rel_2, small.related_3 AS rel_3, small.related_4 AS rel_4,
small.related_5 AS rel_5, small.related_6 AS rel_6, small.related_7 AS rel_7,
small.related_8 AS rel_8, small.related_9 AS rel_9, small.related_10 AS rel_10,
small.related_11 AS rel_11, small.related_12 AS rel_12,
small.related_13 AS rel_13, small.related_14 AS rel_14,
small.related_15 AS rel_15, small.related_16 AS rel_16,
small.related_17 AS rel_17, small.related_18 AS rel_18,
small.related_19 AS rel_19, small.related_20 AS rel_20
MATCH (v:Video {id: videoID})
MATCH (r:Video)
WHERE r.id = rel_1 OR r.id = rel_2 OR r.id = rel_3 OR r.id = rel_4
OR r.id = rel_5 OR r.id = rel_6 OR r.id = rel_7 OR r.id = rel_8
OR r.id = rel_9 OR r.id = rel_10 OR r.id = rel_11 OR r.id = rel_12 OR r.id = rel_13
OR r.id = rel_14 OR r.id = rel_15 OR r.id = rel_16 OR r.id = rel_17
OR r.id = rel_18 OR r.id = rel_19 OR r.id = rel_20
MERGE (r)<-[z: RELATED_TO]-(v)
SET z.strenght = (CASE
  WHEN r.id = rel_1 THEN 20
  WHEN r.id = rel_2 THEN 19
  WHEN r.id = rel_3 THEN 18
  WHEN r.id = rel_4 THEN 17
  WHEN r.id = rel_5 THEN 16
  WHEN r.id = rel_6 THEN 15
  WHEN r.id = rel_7 THEN 14
  WHEN r.id = rel_8 THEN 13
  WHEN r.id = rel_9 THEN 12
  WHEN r.id = rel_10 THEN 11
  WHEN r.id = rel_11 THEN 10
  WHEN r.id = rel_12 THEN 9
  WHEN r.id = rel_13 THEN 8
  WHEN r.id = rel_14 THEN 7
  WHEN r.id = rel_15 THEN 6
  WHEN r.id = rel_16 THEN 5
  WHEN r.id = rel_17 THEN 4
  WHEN r.id = rel_18 THEN 3
  WHEN r.id = rel_19 THEN 2
  WHEN r.id = rel_20 THEN 1
END );

////////////////////////CODE FOR COMMUNITY DETECTION /////////////////////////
///////////////////////////////////////////////////////////////////////////////
// use betweenness as SEEDD
// add betweenness as an attribute
:auto USING PERIODIC COMMIT 5000
LOAD CSV WITH HEADERS FROM "file:///betweenness.csv" AS small
MATCH (v:Video {id: small.Video_ID})
SET v.betweenness = toInteger(small.centrality)

///////////////////////////CREATE A PERSISTENT GRAPH TO SPEDD UP ANALYSIS OF
// SEVERAL MODELS
CALL gds.graph.create(
    'LouvainGraph',
    'Video',
    {
        RELATED_TO: {
            orientation: 'undirected'
        }
    },
    {
        nodeProperties: 'betweenness',
        relationshipProperties: 'strenght'
    }
)

// compute computation statistics
CALL gds.louvain.stats('LouvainGraph',
    { relationshipWeightProperty: 'strenght', seedProperty: 'betweenness' })
YIELD communityCount, modularity, computeMillis, postProcessingMillis

////////////////////////////////////////////////////////////////////////////
// Use category as seed
// Creating a graph and storing it /// we will change the category to an integer
:auto USING PERIODIC COMMIT 20000
MATCH (v:Video)
SET v.catint = (CASE
  WHEN v.category = "Nonprofits & Activism" THEN 1
  WHEN v.category = "Education" THEN 2
  WHEN v.category = "Science & Technology" THEN 3
  WHEN v.category = "Sports" THEN 4
  WHEN v.category = " UNA " THEN 5
  WHEN v.category = "People & Blogs" THEN 6
  WHEN v.category = "Pets & Animals" THEN 7
  WHEN v.category = "Music" THEN 8
  WHEN v.category = "Entertainment" THEN 9
  WHEN v.category = "News & Politics" THEN 10
  WHEN v.category = "Comedy" THEN 11
  WHEN v.category = "Autos & Vehicles" THEN 12
  WHEN v.category = "Howto & Style" THEN 13
  WHEN v.category = "Film & Animation" THEN 14
  WHEN v.category = "Travel & Events" THEN 15
END );

/////////////////// create a persistent graph to run the algowithms with seed
CALL gds.graph.create(
    'LouvainGraph2',
    'Video',
    {
        RELATED_TO: {
            orientation: 'undirected'
        }
    },
    {
        nodeProperties: 'catint',
        relationshipProperties: 'strenght'
    }
)

// call the Louvain algorithm with the categories as seeds
CALL gds.louvain.stats('LouvainGraph2',
    { relationshipWeightProperty: 'strenght', seedProperty: 'catint' })
YIELD communityCount, modularity, computeMillis, postProcessingMillis

// saving the found communities as aproperty of each category
CALL gds.louvain.write('LouvainGraph2',
    { writeProperty: 'communityByCat',
      relationshipWeightProperty: 'strenght',
      seedProperty: 'catint' })
YIELD communityCount, modularity, modularities

/////////////////////////////////GENERAL FUNCTIONS FOR DESCRIPTIVES
// estimate the required memory
CALL gds.louvain.stats.estimate('LouvainGraph',
    { relationshipWeightProperty: 'strenght',
      seedProperty: 'catint' }
)
// write community as property
CALL gds.louvain.write('LouvainGraph',
    { writeProperty: 'weighted_community',
      relationshipWeightProperty: 'strenght' })
YIELD communityCount, modularity, modularities
//calculate number of views and count per community
match(v:Video)
return v.community, avg(distinct(v.views)) as aviews, count(distinct(v.id))
order by aviews

// calculate the avg categories in each community
match(v:Video)
with v.community as com, count(distinct(v.category)) as ccat
return sum(ccat), avg(ccat)
