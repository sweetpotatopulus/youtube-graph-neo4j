/// Calculate intra-category global clustering coefficient
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Travel & Events"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Travel & Events"
                        AND v2.category= "Travel & Events"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

/// NEW ClusteringCoefficient
CALL gds.localClusteringCoefficient.stats(
  {
  nodeQuery: 'MATCH (v1:Video)
                WHERE v1.category= "Comedy"
                RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                        WHERE v1.category= "Comedy"
                          AND v2.category= "Comedy"
                        RETURN id(v1) AS source, id(v2) AS target'
  }
)
YIELD  averageClusteringCoefficient, nodeCount

/// Calculate degree; change "orientation" to get in or out degree
CALL gds.alpha.degree.stream({
  nodeProjection: 'Video',
  relationshipProjection: {
    RELATED_TO: {
      type: 'RELATED_TO',
      orientation: 'REVERSE' // in-degree
    }
  }
})
YIELD nodeId, score
RETURN gds.util.asNode(nodeId).id AS id, gds.util.asNode(nodeId).category AS Category,
      gds.util.asNode(nodeId).age AS Age,  score AS followers
ORDER BY followers DESC

/// Betweenness
CALL gds.alpha.betweenness.stream({
  nodeProjection: 'Video',
  relationshipProjection: 'RELATED_TO'
})
YIELD nodeId, centrality
RETURN gds.util.asNode(nodeId).id AS Video_ID, centrality
ORDER BY centrality DESC

/////////////////////////////////////////////////////////////////////
/// Random Graph Analysis
CALL gds.beta.graph.generate('RandomGraph', 309311, 7,
{
  relationshipDistribution: 'RANDOM',
})
YIELD name, nodes, relationships, averageDegree, relationshipDistribution

/// Random Graph ClusteringCoefficient

CALL gds.localClusteringCoefficient.stats(
  'RandomGraph',
  {
    relationshipTypes:'random_views'
  }
)
YIELD
  averageClusteringCoefficient, nodeCount

/// Random Education graph
CALL gds.beta.graph.generate('RandomEdu', 2320, 1,
{
  relationshipDistribution: 'RANDOM'
})
YIELD name, nodes, relationships, averageDegree

//////////////////////////////////////////////////////////////////
/// Louvain Experiments

CALL gds.graph.create(
    'LouvainWeightViews',
    'Video',
    {
        RELATED_TO: {
            orientation: 'NATURAL'
        }
    },
    {
        relationshipProperties:  'views'
    }
)
//Louvain Second Try //// First Try name: 'LouvainDirectedViews'
                      /// writeProperty: 'weighted_community'
CALL gds.louvain.write('LouvainWeightViews',
    { writeProperty: 'weight_views',
      relationshipWeightProperty: 'views',
      includeIntermediateCommunities: true })
YIELD communityCount, modularity, modularities

//Louvain RESULTS:
//           "communityCount"│"modularity"      │"modularities"                                           │
//          ╞════════════════╪══════════════════╪═════════════════════════════════════════════════════════╡
// 1st TRY  │68318           │0.8379746190851999│[0.662729217944012,0.8084187890831306,0.8379746190851999]│
// 2nd TRY  │72278           │0.8449159967955429│[0.7070758824126726,0.8449159967955429]
// 3rd TRY  │95006           │0.8485109622780651│[0.7126671906821196,0.8485109622780651]
// 3 UNDIR  │28452           │0.8975181355472104│[0.8040288527107555,0.8888812251720395,0.8975181355472104]│

/// RETURN Communities
MATCH (v:Video)
RETURN v.weighted_community[-1], COUNT(DISTINCT(v.id)) AS num_vid
ORDER BY num_vid DESC

/// Retrieve of vids in community
MATCH (v:Video)
WITH v.weighted_community AS comm
WHERE comm[-1]= 86142
RETURN COUNT(DISTINCT(v.id)) AS num_vid

/// Clustering of community
CALL gds.localClusteringCoefficient.stats(
  {
  nodeQuery: 'MATCH (v1:Video)
                WHERE v1.vacomm[-1]= 4101
                RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                        WHERE v1.vacomm[-1]= 4101
                          AND v2.vacomm[-1]= 4101
                        RETURN id(v1) AS source, id(v2) AS target'
  }
)
YIELD  averageClusteringCoefficient, nodeCount


/// Graph with views/age as weight
CALL gds.graph.create(
    'LouvainVW',
    'Video',
    {
        RELATED_TO: {
            orientation: 'UNDIRECTED'
        }
    },
    {
        relationshipProperties:  'wt'
    }
)

/// Louvain with v/a weight
CALL gds.louvain.write('LouvainVW',
    { writeProperty: 'vacomm',
      relationshipWeightProperty: 'wt',
      includeIntermediateCommunities: true })
YIELD communityCount, modularity, modularities

////////////////////////////////////////////////////////////////////
// LABEL Propagation
///general sintax
CALL gds.labelPropagation.write(
  'lwGraph',
  { writeProperty: 'lwLabelp',
    maxIterations: 50,
    relationshipWeightProperty: 'strenght'
  }
)
YIELD
  communityCount, ranIterations, didConverge
