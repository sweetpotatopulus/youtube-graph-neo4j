# Analysis of a YouTube Recommendation Network with Neo4j

This is the repository for the YouTube graph project with Neo4j.

The main objective of this project is to gain insight into the inner workings of Youtube’s recommendation system. We approach this task with the hypothesis that YouTube’s recommendation engine is based mainly on a video’s category (Sports, Comedy, Education etc.). That is to say, that videos of a given category would hardly ever be related to videos from a different category. This hypothesis makes the most intuitive sense judging by the nature of a video.

We will analyze a network of YouTube recommended videos employing the graph database management system **Neo4J**. We also use **Tableau** to create data visualizations. The dataset used in this project was retrieved from an open-source set of YouTube videos from  2008.


## Methodology of the project

 The first step in our analysis consists in verifying if the YouTube network is a random graph. For this, we generate a random graph of analogous properties as the YouTube graph and compare it with our network.

Our study assesses if the clusters in the YouTube network coincide with the main categories of the videos. Thus, the next step in our analysis consists in measuring the clustering coefficient (C) of the categories as if they were the clusters of the network. Then, to assess the suitability of this clustering, we take the category with the worst C and compare it with an analogous random network. With these calculations we set a baseline for assessing the suitability of a clustering and its communities A better clustering should not have communities with a C below the categories’ worst (Cw), and the average C among its communities must be higher than the categories’ average (Cavg).

To obtain an alternative clustering we use the Louvain algorithm. We employ this algorithm in two complementary ways. First, we run the algorithm with initial seed communities. The different seeds correspond to the different categories, and thus serve as the initial communities. When running the algorithm, it will try to keep the seeded communities’ IDs, but it will make the required changes in order to improve the modularity. This represents our first clustering candidate. Second, we run the algorithm without seeds, but considering the weighted relations. In this case, the Louvain algorithm will return a clustering-based only on the link’s properties. This will be a second clustering candidate. And we choose the best among the two.

Another metric that we use for assessing the adequacy of a clustering is modularity. This will provide a measure of the relevance of an alternative community structure. If an alternative clustering not only provides a better C for its communities but it also has a better modularity, then we can consider it a better clustering.

Finally, we try to gain more insight into the formation of the communities by analyzing the main variables that could potentially affect their organization: the views of the videos and their age.

## Conclusions

In this study, we showed that the youtube recommendation graph follows a scale-free structure with preferential attachment. We applied several clustering algorithms to test if the category of a set of videos influences the creation of related video communities. By comparing the modularity on the communities formed by the Louvain algorithm and the average clustering coefficient obtained with triangle counts we discarded our hypothesis and accepted the null hypothesis that the category of a video is not the main factor in the formation of communities by the youtube recommendation system. This conclusion has validity limitations due to the outdated data and the limited number of nodes. Although there is not an exact number, it is estimated to grow in the order of trillions per year.  To get more valid results, a larger graph with more diverse data should be employed. This increase of scale would require a more efficient analytical processing engine, perhaps Pregel based, such as GraphX or a hybrid implementation of Neo4j and GraphX to get both optimized transactions and analytical processing.
