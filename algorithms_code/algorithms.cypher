//calculate intra-category global clustering coefficient
// NEW

CALL gds.localClusteringCoefficient.stats(
  {
  nodeQuery: 'MATCH (v1:Video)
                WHERE v1.category= "Comedy"
                RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                        WHERE v1.category= "Comedy"
                          AND v2.category= "Comedy"
                        RETURN id(v1) AS source, id(v2) AS target'
  }
)
YIELD  averageClusteringCoefficient, nodeCount


//Travel & Events
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Travel & Events"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Travel & Events"
                        AND v2.category= "Travel & Events"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Pets & Animals
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Pets & Animals"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Pets & Animals"
                        AND v2.category= "Pets & Animals"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Film & Animation
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Film & Animation"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Film & Animation"
                        AND v2.category= "Film & Animation"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Autos & Vehicles
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Autos & Vehicles"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Autos & Vehicles"
                        AND v2.category= "Autos & Vehicles"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Howto & Style
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Howto & Style"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Howto & Style"
                        AND v2.category= "Howto & Style"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//People & Blogs
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "People & Blogs"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "People & Blogs"
                        AND v2.category= "People & Blogs"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Nonprofits & Activism
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Nonprofits & Activism"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Nonprofits & Activism"
                        AND v2.category= "Nonprofits & Activism"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Science & Technology
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Science & Technology"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Science & Technology"
                        AND v2.category= "Science & Technology"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//News & Politics
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "News & Politics"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "News & Politics"
                        AND v2.category= "News & Politics"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Education
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Education"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Education"
                        AND v2.category= "Education"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Sports
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Sports"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Sports"
                        AND v2.category= "Sports"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Entertainment
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Entertainment"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Entertainment"
                        AND v2.category= "Entertainment"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Music
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Music"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Music"
                        AND v2.category= "Music"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient

//Comedy
CALL gds.alpha.triangleCount.write({
  nodeQuery: 'MATCH (v1:Video)
              WHERE v1.category= "Comedy"
              RETURN id(v1) AS id',
  relationshipQuery: 'MATCH (v1:Video)-[:RELATED_TO]-(v2:Video)
                      WHERE v1.category= "Comedy"
                        AND v2.category= "Comedy"
                      RETURN id(v1) AS source, id(v2) AS target',
  writeProperty: 'triangle',
  clusteringCoefficientProperty: 'coefficient'
}) YIELD nodeCount, triangleCount, averageClusteringCoefficient
