/////////////////////////////////GENERAL FUNCTIONS
// estimate the required memory
CALL gds.louvain.stats.estimate('LouvainGraph',
    { relationshipWeightProperty: 'strenght',
      seedProperty: 'catint' }
)
// write community as property
CALL gds.louvain.write('LouvainGraph',
    { writeProperty: 'weighted_community',
      relationshipWeightProperty: 'strenght' })
YIELD communityCount, modularity, modularities
//calculate number of views and count per community
match(v:Video)
return v.community, avg(distinct(v.views)) as aviews, count(distinct(v.id))
order by aviews

// avg categories in each community
match(v:Video)
with v.community as com, count(distinct(v.category)) as ccat
return sum(ccat), avg(ccat)

///////////////////////////////////////////////////////////////////////////////
// use betweenness as SEEDD
// add betweenness as an attribute
:auto USING PERIODIC COMMIT 5000
LOAD CSV WITH HEADERS FROM "file:///betweenness.csv" AS small
MATCH (v:Video {id: small.Video_ID})
SET v.betweenness = toInteger(small.centrality)

CALL gds.graph.create(
    'LouvainGraph',
    'Video',
    {
        RELATED_TO: {
            orientation: 'undirected'
        }
    },
    {
        nodeProperties: 'betweenness',
        relationshipProperties: 'strenght'
    }
)

// compute stats
CALL gds.louvain.stats('LouvainGraph',
    { relationshipWeightProperty: 'strenght', seedProperty: 'betweenness' })
YIELD communityCount, modularity, computeMillis, postProcessingMillis

////////////////////////////////////////////////////////////////////////////
// Use category as seed
// Creating a graph and storing it
:auto USING PERIODIC COMMIT 20000
MATCH (v:Video)
SET v.catint = (CASE
  WHEN v.category = "Nonprofits & Activism" THEN 1
  WHEN v.category = "Education" THEN 2
  WHEN v.category = "Science & Technology" THEN 3
  WHEN v.category = "Sports" THEN 4
  WHEN v.category = " UNA " THEN 5
  WHEN v.category = "People & Blogs" THEN 6
  WHEN v.category = "Pets & Animals" THEN 7
  WHEN v.category = "Music" THEN 8
  WHEN v.category = "Entertainment" THEN 9
  WHEN v.category = "News & Politics" THEN 10
  WHEN v.category = "Comedy" THEN 11
  WHEN v.category = "Autos & Vehicles" THEN 12
  WHEN v.category = "Howto & Style" THEN 13
  WHEN v.category = "Film & Animation" THEN 14
  WHEN v.category = "Travel & Events" THEN 15
END );

CALL gds.graph.create(
    'LouvainGraph2',
    'Video',
    {
        RELATED_TO: {
            orientation: 'undirected'
        }
    },
    {
        nodeProperties: 'catint',
        relationshipProperties: 'strenght'
    }
)

CALL gds.louvain.stats('LouvainGraph2',
    { relationshipWeightProperty: 'strenght', seedProperty: 'catint' })
YIELD communityCount, modularity, computeMillis, postProcessingMillis

CALL gds.louvain.write('LouvainGraph2',
    { writeProperty: 'communityByCat',
      relationshipWeightProperty: 'strenght',
      seedProperty: 'catint' })
YIELD communityCount, modularity, modularities

////////////////////////////////////// no clustering /////
CALL gds.louvain.write('LouvainGraph2',
    { writeProperty: 'communityNoCluster',
      relationshipWeightProperty: 'strenght',
      seedProperty: 'catint',
      maxIterations : -1,
      tolerance:1})
YIELD communityCount, modularity
